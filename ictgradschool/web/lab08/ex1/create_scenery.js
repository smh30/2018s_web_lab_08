$(document).ready(function () {

    var d =  $('.dolphin');
    //changing the background when the radio button is clicked
    $("#choose-background").find("input").click(function () {

        var buttonId = $(this).attr('id');
        var split = buttonId.split('-');

        //split is an array with 2 things, 0 = radio, don't need
        var newSrc;
        if (split[1] === "background6") {
            newSrc = split[1] + ".gif";
        } else {
            newSrc = split[1] + ".jpg";
        }

        $("#background").attr("src", "../images/" + newSrc);
    });

    //show/hide dolphins when checkboxes are selected
    //okay i overheard some ideas...................
    //the looping to store data has to happen before hide




    d.hide();
    $('#dolphin1').show();


    $('input[type = checkbox]').click(function () {

        //make an array with all selected dolphins



        //make all the dolphins vanish before just showing the selected ones
        d.hide();
        for (var i of $("input[type=\"checkbox\"]")) {
            if ($(i).prop('checked')) {
                var boxId = $(i).attr('id');
                var split = boxId.split('-');
                //split is an array with 2 things, 0 = radio, don't need
                var selcImgId = split[1];

                //then put back the ones which are selcImgId
                $('#' + selcImgId).show();

            }
        }


    });

    //------------------------the dreaded sliders---------------------------
    // we will need variables to hold the values of teh sliders??
    var x = 0;   // '0' should be value of the #horizontal-control
    var y = 0;   // '0' should be value of the #vertical-control
    var s = 1;   // '0' should be value of teh #size-control


    /// this thing will happen if the value of any slider is changed
    // got help from robin with how to selec the freaking slider and get results out.
    $('input[id="vertical-control"]').change(function () {
        let base = $(this);
        y = base[0].value;
        console.log("y= " + y);
        d.css({"transform": "translateX( " + 0 + "px) translateY(" + 0 + "px) scale(" + 1 + ")"});
        d.css({"transform": "translateX(" + x + "px) translateY(" + -y + "px) scale(" + s + ")"});
    });

    $('input[id="horizontal-control"]').change(function () {
        let base = $(this);
        x = base[0].value;
        console.log("x= " + x);
        d.css({"transform": "translateX( -" + 0 + "px) translateY(" + 0 + "px) scale(" + 1 + ")"});
        d.css({"transform": "translateX(" + x + "px) translateY(" + -y + "px) scale(" + s + ")"});
    });

    $('input[id="size-control"]').change(function () {
        let base = $(this);
        sr = base[0].value;
        if (sr > 0) {
            s = (sr / 50) +1;
        }
        if (sr < 0) {
            s = (1 / ((Math.abs(sr))/50 +1));

        }
        console.log("s= " + s);
        d.css({"transform": "translateX(" + 0 + "px) translateY(" + 0 + "px) scale(" + 1 + ")"});
        d.css({"transform": "translateX(" + x + "px) translateY(" + -y + "px) scale(" + s + ")"});
    });
/// attempt at getting the position of the dolphin to be able to scale the translates across teh full area, unnecessary so ignoring it
   // console.log ("attempt at getting posx: " + $('.dolphin').positionX);

});